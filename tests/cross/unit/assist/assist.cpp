#include <wheels/test/framework.hpp>

#include <twist/run/cross.hpp>

#include <twist/assist/memory.hpp>
#include <twist/assist/preempt.hpp>

#include <cassert>

TEST_SUITE(Memory) {
  struct Widget {
    void Foo() {}
  };

  SIMPLE_TEST(New) {
    twist::run::Cross([] {
      Widget* w = twist::assist::New<Widget>();
      delete w;
    });
  }

  SIMPLE_TEST(AccessObject) {
    twist::run::Cross([] {
      Widget* w = new Widget{};
      twist::assist::MemoryAccess(w, sizeof(Widget));
      delete w;
    });
  }

  SIMPLE_TEST(AccessBuffer) {
    twist::run::Cross([] {
      char* buf = new char[128];

      twist::assist::MemoryAccess(buf, 128);
      twist::assist::MemoryAccess(buf + 64, 16);

      delete[] buf;
    });
  }

  SIMPLE_TEST(Ptr) {
    twist::run::Cross([] {
      twist::assist::Ptr<Widget> w1 = new Widget{};
      w1->Foo();  // operator ->
      (*w1).Foo();  // operator ==

      {
        assert(w1);  // operator bool

        twist::assist::Ptr<Widget> w_nullptr = nullptr;
        assert(!w_nullptr);
      }

      Widget* w2 = w1;  // operator T*

      Widget* w3 = new Widget{};

      twist::assist::Ptr<Widget> w4 = new Widget{};

      // ==
      assert(w1 == w1);  // Ptr<T> == Ptr<T>
      assert(w1 == w2);  // Ptr<T> == T*
      assert(w2 == w1);  // T* == Ptr<T>

      // !=
      assert(w1 != w4);  // Ptr<T> != Ptr<T>
      assert(w1 != w3);  // Ptr<T> != T*
      assert(w3 != w1);  // T* != Ptr<T>

      // operator&
      Widget** wp = &w1;
      (*wp)->Foo();

      delete w1.raw;
      // delete w2;  // w2 == w1
      delete w3;
      delete w4.raw;
    });
  }
}

TEST_SUITE(PreemptionPoint) {
  SIMPLE_TEST(JustWorks) {
    twist::run::Cross([] {
      twist::assist::PreemptionPoint();
    });
  }
}
