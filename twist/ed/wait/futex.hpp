#pragma once

/*
 * Replacement for std::atomic<T>::wait/notify
 * std::atomic<T>::wait/notify api & impl are fundamentally broken
 *
 * Contents
 *   namespace twist::ed::futex
 *     fun Wait
 *     fun WaitTimed
 *     class WakeKey
 *     fun PrepareWake
 *     fun WakeOne
 *     fun WakeAll
 *
 * Usage: examples/futex/main.cpp
 *
 */

#include <twist/rt/facade/wait/futex.hpp>

namespace twist::ed {

namespace futex {

using rt::facade::futex::Wait;
using rt::facade::futex::WaitTimed;

using rt::facade::futex::WakeKey;
using rt::facade::futex::PrepareWake;
using rt::facade::futex::WakeOne;
using rt::facade::futex::WakeAll;

}  // namespace futex

}  // namespace twist::ed
