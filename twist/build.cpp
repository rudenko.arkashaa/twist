#include <twist/single_core.hpp>

// TWIST_FIBERS => TWIST_FAULTY

#if (__TWIST_FIBERS__) && !(__TWIST_FAULTY__)

#error \
    "Invalid twist build configuration: fibers execution backend without fault injection"

#endif

// TWIST_FIBERS_ISOLATE_USER_MEMORY xor sanitizers

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)

#if defined(__has_feature)
#if __has_feature(address_sanitizer) || __has_feature(thread_sanitizer)

#error "Invalid twist build configuration: Memory isolation + AddressSanitizer"

#endif
#endif

#endif

#if defined(__TWIST_NOISY_BUILD__)

#if defined(__TWIST_FIBERS__)
#pragma message("Virtual threads backend: fibers")
#else
#pragma message("Virtual threads backend: threads")
#endif

#if defined(__TWIST_FAULTY__)
#pragma message("Fault injection enabled")
#endif

#if __TWIST_SINGLE_CORE__
#pragma message("Single-core mode")
#else
#pragma message("Multi-core mode")
#endif

#endif
