#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/wait/futex.hpp>

namespace twist::rt::facade {

namespace futex {

using rt::fiber::user::library::futex::Wait;
using rt::fiber::user::library::futex::WaitTimed;

using rt::fiber::user::library::futex::WakeKey;
using rt::fiber::user::library::futex::PrepareWake;

using rt::fiber::user::library::futex::WakeOne;
using rt::fiber::user::library::futex::WakeAll;

}  // namespace futex

}  // namespace twist::rt::facade

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/wait/futex.hpp>

namespace twist::rt::facade {

namespace futex {

using rt::thread::fault::futex::Wait;
using rt::thread::fault::futex::WaitTimed;

using rt::thread::fault::futex::WakeKey;
using rt::thread::fault::futex::PrepareWake;

using rt::thread::fault::futex::WakeOne;
using rt::thread::fault::futex::WakeAll;

}  // namespace futex

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/wait/futex/wait.hpp>

namespace twist::rt::facade {

namespace futex {

using rt::thread::futex::Wait;
using rt::thread::futex::WaitTimed;

using rt::thread::futex::WakeKey;
using rt::thread::futex::PrepareWake;

using rt::thread::futex::WakeOne;
using rt::thread::futex::WakeAll;

}  // namespace futex

}  // namespace twist::rt::facade

#endif
