#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/wait/spin.hpp>

namespace twist::rt::facade {

using rt::fiber::user::SpinWait;
using rt::fiber::user::CpuRelax;

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/wait/spin/wait.hpp>

namespace twist::rt::facade {

using rt::thread::SpinWait;
using rt::thread::CpuRelax;

}  // namespace twist::rt::facade

#endif
