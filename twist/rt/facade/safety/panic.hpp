#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/safety/panic.hpp>

namespace twist::rt::facade {

[[noreturn]] void Panic(
    std::string_view err,
    wheels::SourceLocation loc = wheels::SourceLocation::Current()) {
  fiber::user::Panic(fiber::system::Status::UserAbort, err, loc);
}

}  // namespace twist::rt::facade

#else

#include <wheels/core/panic.hpp>

namespace twist::rt::facade {

using wheels::Panic;

}  // namespace twist::rt::facade

#endif
