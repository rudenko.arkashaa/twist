#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/assist/prune.hpp>

namespace twist::rt::facade {

namespace assist {

using rt::fiber::user::assist::Prune;

}  // namespace assist

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/assist/prune.hpp>

namespace twist::rt::facade {

namespace assist {

using rt::thread::assist::Prune;

}  // namespace assist

}  // namespace twist::rt::facade

#endif
