#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/condition_variable.hpp>

namespace twist::rt::facade::std_like {

using condition_variable = rt::fiber::user::library::std_like::CondVar;  // NOLINT

}  // namespace twist::rt::facade::std_like

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/std_like/condvar.hpp>

namespace twist::rt::facade::std_like {

using condition_variable = rt::thread::fault::FaultyCondVar;  // NOLINT

}  // namespace twist::rt::facade::std_like

#else

#include <condition_variable>

namespace twist::rt::facade::std_like {

using ::std::condition_variable;

}  // namespace twist::rt::facade::std_like

#endif
