#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/thread.hpp>

namespace twist::rt::facade::std_like {

using thread = rt::fiber::user::library::std_like::Thread;  // NOLINT

namespace this_thread = rt::fiber::user::library::std_like::this_thread;

}  // namespace twist::rt::facade::std_like

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/std_like/thread.hpp>

namespace twist::rt::facade::std_like {

using thread = rt::thread::fault::FaultyThread;  // NOLINT

namespace this_thread = rt::thread::fault::this_thread;

}  // namespace twist::rt::facade::std_like

#else

#include <thread>

namespace twist::rt::facade::std_like {

using ::std::thread;

namespace this_thread = ::std::this_thread;

}  // namespace twist::rt::facade::std_like

#endif
