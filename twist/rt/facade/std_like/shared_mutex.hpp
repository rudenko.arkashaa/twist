#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/shared_mutex.hpp>

namespace twist::rt::facade::std_like {

using shared_mutex = rt::fiber::user::library::std_like::SharedMutex;  // NOLINT

}  // namespace twist::rt::facade::std_like

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/std_like/shared_mutex.hpp>

namespace twist::rt::facade::std_like {

using shared_mutex = rt::thread::fault::FaultySharedMutex;  // NOLINT

}  // namespace twist::rt::facade::std_like

#else

#include <shared_mutex>

namespace twist::rt::facade::std_like {

using ::std::shared_mutex;

}  // namespace twist::rt::facade::std_like

#endif
