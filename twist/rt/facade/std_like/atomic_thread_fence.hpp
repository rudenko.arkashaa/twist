#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/atomic_thread_fence.hpp>

namespace twist::rt::facade::std_like {

using rt::fiber::user::library::std_like::atomic_thread_fence;

}  // namespace twist::rt::facade::std_like

#else

#include <atomic>

namespace twist::rt::facade::std_like {

using ::std::atomic_thread_fence;

}  // namespace twist::rt::facade::std_like

#endif
