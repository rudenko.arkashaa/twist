#include "static.hpp"

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

static MemoryMapper mapper;

MemoryMapper* StaticMemoryMapper() {
  return &mapper;
}

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber

#else

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

MemoryMapper* StaticMemoryMapper() {
  WHEELS_PANIC("Not supported in this configuration");
}

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber

#endif
