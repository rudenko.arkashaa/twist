#include "mapper.hpp"

#include <wheels/core/assert.hpp>
#include <wheels/core/panic.hpp>

// std::getenv
#include <cstdlib>
#include <optional>
#include <ios>
#include <sstream>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

//////////////////////////////////////////////////////////////////////

#if defined(__TWIST_FIBERS_FIXED_USER_MEMORY_MAPPING__)

static void* StaticMmapAddress() {
  // https://github.com/llvm/llvm-project/blob/main/compiler-rt/lib/tsan/rtl/tsan_platform.h
  static void* const kAddress = (void*)0x0200000000ULL;

  return kAddress;
}

#endif

//////////////////////////////////////////////////////////////////////

static std::optional<void*> ParseHexAddr(char* addr_str) {
  try {
    std::stringstream ss;
    ss << addr_str;

    uintptr_t addr;
    ss >> std::hex >> addr;
    if (ss.eof()) {
      return (void*)addr;
    } else {
      return std::nullopt;
    }
  } catch (...) {
    return std::nullopt;
  }
}

static void* TryGetMmapAddressFromEnv() {
  static const char* kAddressVar = "TWIST_USER_MEMORY_ADDRESS";

  char* addr_str = std::getenv(kAddressVar);

  if (!addr_str) {
    return nullptr;  // Not presented
  }

  if (auto addr = ParseHexAddr(addr_str)) {
    return *addr;
  } else {
    WHEELS_PANIC("Cannot parse " << kAddressVar << ": " << addr_str);
  }
}

//////////////////////////////////////////////////////////////////////

static void* SelectMmapAddress() {
#if defined(__TWIST_FIBERS_FIXED_USER_MEMORY_MAPPING__)
  return StaticMmapAddress();
#endif

  if (void* env_addr = TryGetMmapAddressFromEnv()) {
    return env_addr;
  }

  return nullptr;  // Arbitrary
}

//////////////////////////////////////////////////////////////////////

static const size_t kMmapSize = 1024 * 1024 * 1024;

//////////////////////////////////////////////////////////////////////

MemoryMapper::MemoryMapper() {
  Mmap();
}

void MemoryMapper::Mmap() {
  size_t pages = 1 + kMmapSize / wheels::MmapAllocation::PageSize();
  void* address = SelectMmapAddress();

  auto map = wheels::MmapAllocation::AllocatePages(pages, /*hint=*/address);

  if (address != nullptr) {
    WHEELS_VERIFY(map.Start() == address,
                  "Cannot mmap requested memory address");
  }

  map_ = std::move(map);
  fixed_address_ = address != nullptr;
}

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
