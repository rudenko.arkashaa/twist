#pragma once

#include "mapper.hpp"

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

MemoryMapper* StaticMemoryMapper();

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
