#pragma once

namespace twist::rt::fiber {

namespace system {

struct Fiber;
class Simulator;

}  // namespace system

}  // namespace twist::rt::fiber
