#pragma once

#include "atomic.hpp"
#include "non_atomic.hpp"
#include "object_index.hpp"

#include "../fiber/struct.hpp"
#include "../thread_count.hpp"

#include <twist/rt/fiber/system/object_allocator.hpp>

namespace twist::rt::fiber {

namespace system::sync {

// A Promising Semantics for Relaxed-Memory Concurrency
// https://sf.snu.ac.kr/publications/promising.pdf

class SeqCstMemoryModel {
 public:
  SeqCstMemoryModel() {
  }

  void Reset() {
    atomics_.Reset();
    non_atomics_.Reset();
  }

  // Threads

  void Tick(Fiber* fiber) {
    fiber->sync.clock.current.Tick(fiber->id);
    fiber->sync.clock.epoch += 1;
  }

  void SpawnFiber(Fiber* parent, Fiber* child) {
    child->sync.clock.current.Assign(parent->sync.clock.current);
    Tick(child);
  }

  void WakeFiber(Fiber* waker, Fiber* waiter) {
    // Wake acts as release operation
    waiter->sync.clock.current.Join(waker->sync.clock.release);
    Tick(waiter);
  }

  // Sync

  uint64_t Sync(Fiber* fiber, Action* action) {
    Tick(fiber);

    switch (action->type) {
      case ActionType::AtomicInit:
        AtomicInit(fiber, action);
        return 0;
      case ActionType::AtomicDestroy:
        AtomicDestroy(fiber, action);
        return 0;
      case ActionType::AtomicLoad:
        return AtomicLoad(fiber, action);
      case ActionType::AtomicDebugLoad:
        return AtomicDebugLoad(fiber, action);
      case ActionType::AtomicStore:
        AtomicStore(fiber, action);
        return 0;
      case ActionType::AtomicRmwLoad:
        return AtomicRmwLoad(fiber, action);
      case ActionType::AtomicRmwCommit:
        AtomicRmwCommit(fiber, action);
        return 0;
      case ActionType::AtomicThreadFence:
        AtomicThreadFence(fiber, action);
        return 0;
      default:
        WHEELS_PANIC("Unhandled synchronization action");
    }
  }

  // Futex

  uint64_t FutexLoad(Fiber* /*fiber*/, AtomicVar* atomic) {
    return atomic->last_store.value;
  }

  AtomicVar* FutexAtomic(void* loc) {
    return GetAtomic(loc);
  }

  AtomicVar* TryFutexAtomic(void* loc) {
    return TryGetAtomic(loc);
  }

  // Non-atomics

  std::optional<OldAccess> AccessNonAtomicVar(Fiber* fiber, Access access) {
    Tick(fiber);

    NonAtomicVar* var;
    if (access.type == AccessType::Init) {
      var = SystemAllocator::Get()->Allocate<NonAtomicVar>();
      var->Init();
      var->source_loc = access.source_loc;
      non_atomics_.Insert(access.loc, var);
    } else {
      var = non_atomics_.Access(access.loc);
      WHEELS_VERIFY(var != nullptr, "Non-atomic var not found");
    }

    if (auto race = CheckDataRaces(fiber, var, access)) {
      return race;
    }

    if (access.type == AccessType::Destroy) {
      non_atomics_.Remove(access.loc);
      SystemAllocator::Get()->Free(var);
    } else {
      var->Log({
          access.loc,
          access.type,
          fiber->id,
          fiber->sync.clock.epoch,
          access.source_loc});
    }

    return std::nullopt;
  }

 private:
  // Atomics

  AtomicVar* TryGetAtomic(void* loc) {
    return atomics_.Access(loc);
  }

  AtomicVar* GetAtomic(void* loc) {
    AtomicVar* a = TryGetAtomic(loc);
    WHEELS_VERIFY(a != nullptr, "Atomic not found");
    return a;
  }

  void AtomicInit(Fiber*, Action* init) {
    AtomicVar* atomic = SystemAllocator::Get()->Allocate<AtomicVar>();
    atomics_.Insert(init->loc, atomic);

    atomic->source_loc = init->source_loc;
    atomic->last_store.value = init->value;
    atomic->last_store.clock.Init();  // Initialization is not atomic
    atomic->futex.Init();
  }

  uint64_t AtomicLoad(Fiber* reader, Action* load) {
    WHEELS_ASSERT(IsLoadOrder(load->mo), "Unexpected memory order for atomic load");

    AtomicVar* atomic = GetAtomic(load->loc);

    // Acquire load ~ Relaxed load ; Acquire fence
    uint64_t value = AtomicLoadRelaxed(reader, atomic);
    if (IsAcquireOrder(load->mo)) {
      AtomicThreadFenceAcquire(reader);
    }

    return value;
  }

  uint64_t AtomicDebugLoad(Fiber* /*reader*/, Action* load) {
    AtomicVar* atomic = GetAtomic(load->loc);
    return atomic->last_store.value;
  }

  void AtomicStore(Fiber* writer, Action* store) {
    WHEELS_ASSERT(IsStoreOrder(store->mo), "Unexpected memory order for atomic store");

    AtomicVar* atomic = GetAtomic(store->loc);

    // Atomic store ~ Release fence ; Relaxed store
    if (IsReleaseOrder(store->mo)) {
      AtomicThreadFenceRelease(writer);
    }
    AtomicStoreRelaxed(writer, atomic, store->value);
  }

  uint64_t AtomicRmwLoad(Fiber* writer, Action* rmw) {
    AtomicVar* atomic = GetAtomic(rmw->loc);

    uint64_t value = AtomicLoadRelaxed(writer, atomic);
    if (IsAcquireOrder(rmw->mo)) {
      AtomicThreadFenceAcquire(writer);
    }
    return value;
  }

  void AtomicRmwCommit(Fiber* writer, Action* rmw) {
    AtomicVar* atomic = GetAtomic(rmw->loc);

    if (IsReleaseOrder(rmw->mo)) {
      AtomicThreadFenceRelease(writer);
    }
    AtomicRmwCommitRelaxed(writer, atomic, rmw->value);
  }

  void AtomicDestroy(Fiber*, Action* destroy) {
    AtomicVar* atomic = GetAtomic(destroy->loc);
    WHEELS_VERIFY(atomic->futex.IsEmpty(), "Futex queue is not empty");
    atomics_.Remove(destroy->loc);
    SystemAllocator::Get()->Free(atomic);
  }

  static uint64_t AtomicLoadRelaxed(Fiber* reader, AtomicVar* atomic) {
    reader->sync.clock.acquire.Join(atomic->last_store.clock);
    return atomic->last_store.value;
  }

  static void AtomicStoreRelaxed(Fiber* writer, AtomicVar* atomic, uint64_t value) {
    atomic->last_store.value = value;
    atomic->last_store.clock.Assign(writer->sync.clock.release);
  }

  static void AtomicRmwCommitRelaxed(Fiber* writer, AtomicVar* atomic, uint64_t value) {
    atomic->last_store.clock.Join(writer->sync.clock.release);
    atomic->last_store.value = value;
  }

  // Fences

  static void AtomicThreadFenceRelease(Fiber* fiber) {
    // release < current
    fiber->sync.clock.release.Assign(fiber->sync.clock.current);
  }

  static void AtomicThreadFenceAcquire(Fiber* fiber) {
    fiber->sync.clock.current.Join(fiber->sync.clock.acquire);
  }

  static void AtomicThreadFenceAcqRel(Fiber* fiber) {
    AtomicThreadFenceAcquire(fiber);
    AtomicThreadFenceRelease(fiber);
  }

  static void AtomicThreadFence(Fiber* fiber, Action* fence) {
    switch (fence->mo) {
      case std::memory_order::acquire:
        AtomicThreadFenceAcquire(fiber);
        return;
      case std::memory_order::release:
        AtomicThreadFenceRelease(fiber);
        return;
      case std::memory_order::acq_rel:
        AtomicThreadFenceAcqRel(fiber);
        return;
      default:
        WHEELS_PANIC("Fence not supported");
    }
  }

 private:
  // Non-atomics

  static bool Conflict(AccessType a, AccessType b) {
    return IsWrite(a) || IsWrite(b);
  }

  static bool HappensBefore(const OldAccess& old, const Fiber* fiber) {
    return fiber->sync.clock.current.Get(old.fiber) >= old.epoch;
  }

  std::optional<OldAccess> CheckDataRaces(const Fiber* fiber, NonAtomicVar* var, const Access& curr) {
    if (var->last_write && (var->last_write->fiber != fiber->id)) {
      // Compare current access with the last write (from different thread)
      OldAccess& last_write = *(var->last_write);
      if (!HappensBefore(last_write, fiber)) {
        return last_write;
      }
    }

    if (IsWrite(curr.type)) {
      // Compare current write with the last read from each thread

      size_t n = ThreadCount();
      for (size_t i = 1; i <= n; ++i) {
        if ((i != fiber->id) && (var->last_read[i])) {
          OldAccess& old = *(var->last_read[i]);
          if (!HappensBefore(old, fiber)) {
            return old;
          }
        }
      }
    }

    return std::nullopt;
  }

 private:
  ObjectIndex<void*, AtomicVar> atomics_;
  ObjectIndex<void*, NonAtomicVar> non_atomics_;
};

}  // namespace system::sync

}  // namespace twist::rt::fiber
