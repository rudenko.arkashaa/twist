#pragma once

#include "atomic.hpp"
#include "../resource_manager.hpp"

#include <optional>
#include <vector>
#include <utility>

namespace twist::rt::fiber {

namespace system::sync {

// Key -> Object* mapping

// Simple linear probing
// TODO: support rehashing

template <typename Key, typename Obj>
class ObjectIndex {
  struct Entry {
    std::optional<Key> key;
    Obj* obj = nullptr;
  };

  static const size_t kInitTableSize = 137;

 public:
  ObjectIndex() {
    table_.resize(kInitTableSize);
  }

  void Insert(Key key, Obj* obj) {
    size_t i = Lookup(key);
    if (!table_[i].key) {
      table_[i] = {key, obj};
    } else {
      wheels::Panic("Object already exits");
    }
  }

  Obj* Access(Key key) {
    size_t i = Lookup(key);
    if (table_[i].key) {
      return table_[i].obj;
    } else {
      return nullptr;
    }
  }

  Obj* Remove(Key key) {
    size_t i = Lookup(key);
    if (table_[i].key == key) {
      table_[i].key.reset();
      return std::exchange(table_[i].obj, nullptr);
    } else {
      return nullptr;
    }
  }

  void Reset() {
    for (size_t i = 0; i < table_.size(); ++i) {
      if (table_[i].key) {
        table_[i].key.reset();
      }
    }
  }

 private:
  // Or first empty slot
  size_t Lookup(Key key) const {
    size_t i = Hash(key) % table_.size();

    while (table_[i].key) {
      if (table_[i].key == key) {
        return i;
      } else {
        i = (i + 1) % table_.size();
      }
    }

    return i;  // Not found, first empty slot
  }

  static size_t Hash(Key key) {
    return std::hash<Key>()(key);
  }

 private:
  std::vector<Entry> table_;
};

}  // namespace system::sync

}  // namespace twist::rt::fiber
