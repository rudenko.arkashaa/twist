#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

inline static const size_t kMaxFibers = 16;

}  // namespace twist::rt::fiber
