#pragma once

#include <functional>

namespace twist::rt::fiber {

namespace system {

using MainRoutine = std::function<void()>;

}  // namespace system

}  // namespace twist::rt::fiber
