#pragma once

#include <map>
#include <typeinfo>

namespace twist::rt::fiber {

namespace system {

using SystemObjectTypeId = size_t;

struct SystemObject {
  SystemObject* next = nullptr;
  SystemObject* pool_next = nullptr;

  virtual ~SystemObject() = default;
};

template <typename T>
SystemObjectTypeId GetSystemObjectTypeId() {
  return typeid(T).hash_code();
}

template <typename T>
struct TypedSystemObject : SystemObject {
  TypedSystemObject() {
  }
};

class SystemObjectPool {
 public:
  void Push(SystemObject* obj) {
    obj->pool_next = top_;
    top_ = obj;
  }

  SystemObject* TryPop() {
    if (top_ != nullptr) {
      SystemObject* top = top_;
      top_ = top_->pool_next;
      return top;
    } else {
      return nullptr;
    }
  }

  bool IsEmpty() const {
    return top_ == nullptr;
  }

  void HardReset() {
    top_ = nullptr;
  }

  ~SystemObjectPool() {
    SystemObject* obj = top_;
    while (obj != nullptr) {
      SystemObject* next_obj = obj->next;
      delete obj;
      obj = next_obj;
    }
  }

 private:
  SystemObject* top_ = nullptr;
};

class SystemObjectClassAllocator {
 public:
  void New(SystemObject* obj) {
    obj->next = head_;
    head_ = obj;
  }

  SystemObject* TryAlloc() {
    return pool_.TryPop();
  }

  void Free(SystemObject* obj) {
    pool_.Push(obj);
  }

  void Reset() {
    pool_.HardReset();

    SystemObject* obj = head_;
    while (obj != nullptr) {
      pool_.Push(obj);
      obj = obj->next;
    }
  }

 private:
  SystemObject* head_ = nullptr;
  SystemObjectPool pool_;
};

class SystemAllocator {
 public:
  template <typename T>
  T* Allocate() {
    auto id = GetSystemObjectTypeId<T>();
    SystemObject* obj = allocs_[id].TryAlloc();
    if (obj == nullptr) {
      obj = new T{};
      allocs_[id].New(obj);
    }
    return static_cast<T*>(obj);
  }

  template <typename T>
  void Free(T* obj) {
    auto id = GetSystemObjectTypeId<T>();
    allocs_[id].Free(obj);
  }

  void Reset() {
    for (auto& [_, alloc] : allocs_) {
      alloc.Reset();
    }
  }

  static SystemAllocator* Get();

  ~SystemAllocator() {
    // No-op
  }

 private:
  std::map<SystemObjectTypeId, SystemObjectClassAllocator> allocs_;
};

}  // namespace system

}  // namespace twist::rt::fiber
