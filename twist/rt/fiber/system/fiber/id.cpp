#include "../id_allocator.hpp"

namespace twist::rt::fiber {

namespace system {

const FiberId kImpossibleFiberId = IdAllocator::kImpossibleId;

}  // namespace system

}  // namespace twist::rt::fiber
