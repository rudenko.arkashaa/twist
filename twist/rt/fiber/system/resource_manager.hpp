#pragma once

#include "object_allocator.hpp"

namespace twist::rt::fiber {

namespace system {

class ResourceManager : public SystemAllocator {
 public:
  ResourceManager() {
    Warmup();
  }

  ~ResourceManager();

 private:
  void Warmup();

 private:
  //
};

ResourceManager* StaticResourceManager();

}  // namespace system

}  // namespace twist::rt::fiber
