#pragma once

#include "../fwd.hpp"
#include "../limits.hpp"

#include "run_queue.hpp"
#include "wait_queue.hpp"

#include <cstdint>
#include <cstdlib>

namespace twist::rt::fiber {

namespace system::scheduler {

struct IScheduler : IRunQueue {
  virtual ~IScheduler() = default;

  virtual bool NextSchedule() = 0;

  virtual void Start(Simulator*) = 0;

  virtual void Interrupt(Fiber*) = 0;
  virtual void Yield(Fiber*) = 0;
  virtual void Wake(Fiber*) = 0;
  virtual void Spawn(Fiber*) = 0;
  virtual void Exit(Fiber*) = 0;

  // Hints

  virtual void LockFree(Fiber*, bool flag) = 0;
  virtual void Progress(Fiber*) = 0;
  virtual void NewIter() = 0;

  // Futex

  virtual IWaitQueuePtr NewWaitQueue() = 0;

  // Random

  virtual uint64_t RandomNumber() = 0;
  virtual size_t RandomChoice(size_t alts) = 0;

  // Spurious

  virtual bool SpuriousWakeup() = 0;

  // mutex::try_lock, atomic<T>::compare_exchange_weak
  virtual bool SpuriousTryFailure() = 0;
};

}  // namespace system::scheduler

}  // namespace twist::rt::fiber
