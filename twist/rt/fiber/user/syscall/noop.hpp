#pragma once

namespace twist::rt::fiber {

namespace user::syscall {

void Noop();

}  // namespace user::syscall

}  // namespace twist::rt::fiber
