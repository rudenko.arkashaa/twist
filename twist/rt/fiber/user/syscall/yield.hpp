#pragma once

namespace twist::rt::fiber {

namespace user::syscall {

void Yield();

}  // namespace user::syscall

}  // namespace twist::rt::fiber
