#pragma once

#include <twist/rt/fiber/system/time.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

void SleepFor(system::Time::Duration delay);
void SleepUntil(system::Time::Instant deadline);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
