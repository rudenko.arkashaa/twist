#pragma once

#include <twist/rt/fiber/system/status.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

[[noreturn]] void Abort(system::Status);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
