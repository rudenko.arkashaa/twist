#pragma once

#include <twist/rt/fiber/system/futex/wake_count.hpp>
#include <twist/rt/fiber/system/futex/waiter.hpp>
#include <twist/rt/fiber/system/call/status.hpp>
#include <twist/rt/fiber/system/time.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

system::call::Status FutexWait(void* var, uint64_t val, system::WaiterContext*);
system::call::Status FutexWaitTimed(void* var, uint64_t val, system::Time::Instant, system::WaiterContext*);
void FutexWake(void* var, size_t);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
