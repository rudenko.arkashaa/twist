#include "memory.hpp"

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>

#include "../safety/panic.hpp"

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)

#include <twist/rt/fiber/system/simulator.hpp>

#include "../library/fmt/temp.hpp"

namespace twist::rt::fiber {

namespace user::assist {

void MemoryAccess(void* addr, size_t size, wheels::SourceLocation call_site) {
  if (addr == nullptr) {
    user::Panic(system::Status::MemoryAccess, "Nullptr access", call_site);
  }

  scheduler::Interrupt();

  if (!system::Simulator::Current()->UserMemoryAccess(addr, size)) {
    user::Panic(system::Status::MemoryAccess,
        user::library::fmt::FormatTemp(
            "Cannot access heap at address = {}, size = {}", addr, size), call_site);
  }
}

}  // namespace user::assist

}  // namespace twist::rt::fiber

#else

namespace twist::rt::fiber {

namespace user::assist {

void MemoryAccess(void* ptr, size_t /*size*/, wheels::SourceLocation call_site) {
  if (ptr == nullptr) {
    user::Panic(system::Status::MemoryAccess, "Nullptr access", call_site);
  }

  scheduler::Interrupt();

  // TODO: read memory?
}

}  // namespace user::assist

}  // namespace twist::rt::fiber

#endif