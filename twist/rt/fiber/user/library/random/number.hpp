#pragma once

#include <twist/rt/fiber/user/syscall/random.hpp>

namespace twist::rt::fiber {

namespace user::library::random {

inline uint64_t Number() {
  return syscall::RandomNumber();
}

}  // namespace user::library::random

}  // namespace twist::rt::fiber
