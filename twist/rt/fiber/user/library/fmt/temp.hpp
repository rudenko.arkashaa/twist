#pragma once

#include <fmt/core.h>

#include <cstdlib>

namespace twist::rt::fiber {

namespace user::library::fmt {

static inline constexpr size_t kBufferSize = 1024;

inline char* StaticFormatBuffer() {
  static char buf[kBufferSize];
  return buf;
}

template <typename... Args>
std::string_view FormatTemp(::fmt::format_string<Args...> format_str, Args&&... args) {
  char* buffer = StaticFormatBuffer();
  auto written = ::fmt::format_to_n(buffer, kBufferSize, format_str, ::std::forward<Args>(args)...);
  return {buffer, written.size};
}

template <typename... Args>
std::string_view FormatLnTemp(::fmt::format_string<Args...> format_str, Args&&... args) {
  char* buffer = StaticFormatBuffer();

  // Message
  auto message = ::fmt::format_to_n(buffer, kBufferSize - 3, format_str, ::std::forward<Args>(args)...);
  size_t size = message.size;

  // Line break
  size_t space_left = kBufferSize - size;
  auto line_break = ::fmt::format_to_n(buffer + message.size, space_left, "\n");
  size += line_break.size;

  return {buffer, size};
}

}  // namespace user::library::fmt

}  // namespace twist::rt::fiber
