#pragma once

#include <twist/rt/thread/fault/std_like/atomic.hpp>

#include <twist/rt/thread/wait/futex/wait.hpp>

#include <cstdint>

namespace twist::rt::thread::fault {

namespace futex {

void Wait(FaultyAtomic<uint32_t>& atomic, uint32_t old,
          std::memory_order mo = std::memory_order::seq_cst);

bool WaitTimed(FaultyAtomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout,
               std::memory_order mo = std::memory_order::seq_cst);

using rt::thread::futex::WakeKey;

WakeKey PrepareWake(FaultyAtomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace futex

}  // namespace twist::rt::thread::fault
