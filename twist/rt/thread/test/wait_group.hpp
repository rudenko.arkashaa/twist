#pragma once

#include <twist/rt/thread/wait/futex/wait.hpp>

#include <atomic>
#include <thread>
#include <vector>

namespace twist::rt::thread {

namespace test {

class WgLatch {
 public:
  void Wait() {
    futex::Wait(released_, 0);
  }

  void Release() {
    auto wake_key = futex::PrepareWake(released_);
    released_.store(1);
    futex::WakeAll(wake_key);
  }

 private:
  std::atomic<uint32_t> released_{0};
};

class WaitGroup {
 public:
  template <typename F>
  WaitGroup& Add(F fn) {
    threads_.emplace_back([this, fn = std::move(fn)]() mutable {
      start_latch_.Wait();
      fn();
    });
    return *this;
  }

  template <typename F>
  WaitGroup& Add(size_t count, F fn) {
    for (size_t i = 0; i < count; ++i) {
      Add(fn);
    }
    return *this;
  }

  void Join() {
    {
      // Start participants
      start_latch_.Release();
    }

    {
      // Join participants
      for (auto& t : threads_) {
        t.join();
      }
      threads_.clear();
    }
  }

 private:
  WgLatch start_latch_;
  std::vector<std::thread> threads_;
};

}  // namespace test

}  // namespace twist::rt::thread
