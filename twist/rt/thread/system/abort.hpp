#pragma once

#include <cstdlib>

namespace twist::rt::thread {

[[noreturn]] inline void Abort() {
  std::abort();
}

}  // namespace twist::rt::thread
