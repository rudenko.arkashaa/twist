#pragma once

#include <unistd.h>

#include <string_view>

namespace twist::rt::thread {

inline void Write(int fd, std::string_view buf) {
  ::write(fd, buf.data(), buf.size());
}

}  // namespace twist::rt::thread
