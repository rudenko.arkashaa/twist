#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/pct/scheduler.hpp>

#include <optional>

namespace twist::sim::sched {

namespace pct {

using twist::rt::fiber::system::scheduler::pct::Scheduler;
using SchedulerParams = twist::rt::fiber::system::scheduler::pct::Params;

}  // namespace pct

}  // namespace twist::sim::sched
