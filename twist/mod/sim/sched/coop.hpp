#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/coop/scheduler.hpp>

namespace twist::sim::sched {

namespace coop {

using twist::rt::fiber::system::scheduler::coop::Scheduler;
using SchedulerParams = twist::rt::fiber::system::scheduler::coop::Params;

}  // namespace coop

}  // namespace twist::sim::sched
