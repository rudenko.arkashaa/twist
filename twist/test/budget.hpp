#pragma once

#include <wheels/core/stop_watch.hpp>

#include <algorithm>

#include <chrono>

namespace twist::test {

// Single-threaded!

class TimeBudget {
  using Clock = std::chrono::steady_clock;

 public:
  TimeBudget(std::chrono::seconds secs)
    : secs_(secs) {
    start_ = Clock::now();
  }

  Clock::duration Left() const {
    auto now = Clock::now();
    auto expiration = start_ + secs_;
    if (now > expiration) {
      return std::chrono::seconds(0);
    } else {
      // expiration >= now
      return expiration - now;
    }
  }

  // Single-threaded
  class Client {
    using Millis = std::chrono::milliseconds;

   public:
    static constexpr Millis kBatchThreshold = Millis(10);

    Client(TimeBudget* host)
        : host_(host) {
    }

    bool Test() {
      static const Millis kSafeMargin = Millis(250);

      ++count_;
      if (count_ == batch_) {
        Adapt();
        return host_->Left() > kSafeMargin;
      } else {
        return true;
      }
    }

   private:
    void Adapt() {
      auto elapsed = batch_timer_.Elapsed();

      if (elapsed * 2 < kBatchThreshold) {
        batch_ *= 2;
      } else {
        // Restart batch
        batch_timer_.Restart();
        count_ = 0;

        if (elapsed > kBatchThreshold * 2) {
          batch_ = std::max(batch_ / 2, (size_t)1);
        }
      }
    }

   private:
    TimeBudget* host_;
    wheels::StopWatch<> batch_timer_;
    size_t count_ = 0;
    size_t batch_ = 1;
  };

  Client MakeClient() {
    return {this};
  }

 private:
  const std::chrono::seconds secs_;
  Clock::time_point start_;
};

}  // namespace twist::test
