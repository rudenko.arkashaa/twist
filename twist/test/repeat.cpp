#include <twist/test/repeat.hpp>

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::test {

void Repeat::NewIterHint() {
  rt::fiber::system::Simulator::Current()->SysSchedHintNewIter();
}

}  // namespace twist::test

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/adversary/adversary.hpp>

namespace twist::test {

void Repeat::NewIterHint() {
  rt::thread::fault::Adversary()->Iter(iter_);
}

}  // namespace twist::test

#else

namespace twist::test {

void Repeat::NewIterHint() {
  // No-op
}

}  // namespace twist::test

#endif
