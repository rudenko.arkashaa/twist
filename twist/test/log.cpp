#include "log.hpp"

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::test {

namespace log {

bool Enabled() {
  return true;
}

wheels::MutableMemView FmtBuffer() {
  static const size_t kSize = 512;
  // NB: static, do not affect addresses of local variables on thread stack
  static char buf[kSize];
  return {buf, kSize};
}

void Log(wheels::SourceLocation loc, std::string_view message) {
  rt::fiber::system::Simulator::Current()->SysLog(loc, message);
}

}  // namespace log

}  // namespace twist::test

#else

#include <twist/rt/thread/logging/logging.hpp>

namespace twist::test {

namespace log {

bool Enabled() {
  return true;
}

wheels::MutableMemView FmtBuffer() {
  static const size_t kSize = 512;
  static thread_local char buf[kSize];
  return {buf, kSize};
}

void Log(wheels::SourceLocation loc, std::string_view line) {
  rt::thread::log::LogMessage(loc, line);
}
}  // namespace log

}  // namespace twist::test

#endif
