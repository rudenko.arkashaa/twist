#pragma once

#include <twist/rt/fiber/scheduler/random/scheduler.hpp>

namespace twist::run::sim {

namespace random {

using Scheduler = rt::fiber::system::scheduler::random::Scheduler;

using Params = Scheduler::Params;

}  // namespace random

}  // namespace twist::run::sim
