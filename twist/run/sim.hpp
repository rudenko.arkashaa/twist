#pragma once

#include <twist/build.hpp>

#include <twist/run/main.hpp>

#include <twist/run/sim/simulator.hpp>
#include <twist/run/sim/scheduler.hpp>
#include <twist/run/sim/params.hpp>
#include <twist/run/sim/det_check.hpp>

static_assert(twist::build::Sim());

namespace twist::run {

sim::Result Sim(sim::Params params, MainRoutine main);

// Default parameters, fixed random schedule
sim::Result Sim(MainRoutine main);

}  // namespace twist::run
