#pragma once

/*
 * Preemption point for thread adversary / fiber scheduler
 *
 * void twist::assist::PreemptionPoint();
 */

#include <twist/rt/facade/assist/preempt.hpp>

namespace twist::assist {

using rt::facade::assist::PreemptionPoint;

}  // namespace twist::assist
