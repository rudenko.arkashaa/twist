# Twist

Задача _Twist_ – быстро и воспроизводимо проявить ошибки многопоточной синхронизации.

## Техники

Две главные техники, которые использует _Twist_, – это:

- внедрение сбоев (_fault injection_) и
- детерминированная симуляция.

### Fault injection

В точках обращения потоков к примитивам синхронизации _Twist_ внедряет "сбои" – псевдо-случайные переключения контекста,
что увеличивает покрытие графа состояний теста.

### Детерминированная симуляция

Недетерминированные многопоточные исполнения моделируются в _Twist_ с помощью однопоточных кооперативных файберов, что позволяет
- Детерминированно воспроизводить недетерминированные по своей природе баги
- Рандомизировать очередь планировщика и очереди ожидания в мьютексах и кондварах (в дополнение к внедрению сбоев)
- Вместить больше симуляций в ограниченный бюджет времени стресс-теста

## Навигация

Клиентская часть библиотеки – директория `twist/ed/`, пространство имен `twist::ed`

- `std/` – замена стандартным примитивам синхронизации
- `wait/` – механизмы ожидания
- `static/` – макросы для объявления статических переменных
  - `thread_local/` – статических `thread_local` переменных

Среда исполнения – директория `twist/rt/`, пространство имен `twist::rt`

### Правила
- Если вы пишете многопоточный код, то вам нужен **только** `twist/ed/`. 
- Если вы пишете тест для многопоточного кода, то вам нужен еще и `twist/rt/run/`

## Правила использования

Чтобы _Twist_ мог корректно работать и ловить баги, пользователь должен соблюдать следующие правила:

### Примитивы

Вместо `std::thread`, `std::atomic`, `std::mutex`, `std::condition_variable` и других примитивов из [Thread support library](https://en.cppreference.com/w/cpp/thread) необходимо использовать одноименные примитивы из пространства имен `twist::ed::std`.

Примитивы из _Twist_ повторяют интерфейсы и сохраняют поведение примитивов из стандартной библиотеки, так что вы можете пользоваться документацией https://en.cppreference.com/w/ и **не думать** про _Twist_.

Заголовочные файлы меняются по следующему принципу: `#include <atomic>` заменяется на `#include <twist/ed/std/atomic.hpp>`

Доступные заголовки: [twist/ed/std](/twist/ed/std)

При этом можно использовать `std::lock_guard` и `std::unique_lock` (но только в связке с `twist::ed::std::mutex`), это не примитивы синхронизации, а RAII для более безопасного использования мьютекса.

Использование примитивов из `::std` приведет к неопределенному поведению в тестах, будьте внимательны!

### Планировщик

Заголовочный файл: `twist/ed/std/thread.hpp`

#### `yield`

Вместо `std::this_thread::yield` нужно использовать `twist::ed::std::this_thread::yield`.

А еще лучше использовать `twist::ed::SpinWait` из заголовочного файла `<twist/ed/wait/spin.hpp>`.

#### `sleep_for`

Вместо `std::this_thread::sleep_for` нужно использовать `twist::ed::std::this_thread::sleep_for`.

### Ожидание

_Twist_ предоставляет два механизма ожидания:
- активный (`SpinWait`) и
- блокирующий (`futex::Wait`).

#### `SpinWait`

Заголовочный файл: `<twist/ed/wait/spin.hpp>`

Предназначен для _адаптивного_ _активного ожидания_ (_busy waiting_) в спинлоках.

##### Пример

[examples/spin/main.cpp](/examples/spin/main.cpp)

```cpp
void SpinLock::Lock() {
  // Одноразовый!
  // Для каждого нового цикла ожидания в каждом потоке 
  // нужно заводить отдельный экземпляр SpinWait
  twist::ed::SpinWait spin_wait;
  while (locked_.exchange(true)) {
    // У SpinWait определен operator()
    spin_wait();  // Exponential backoff
  }
}
```

#### Futex

Заголовочный файл: `twist/ed/wait/futex.hpp`

Для блокирующего ожидания вместо методов `wait` / `notify_{one,all}` у `std::atomic` нужно использовать
свободные функции из пространства имен `twist::ed::futex::`: 
- `Wait` / `WaitTimed`
- `PrepareWake` + `Wake{One,All}`

##### Пример

[examples/futex/main.cpp](/examples/futex/main.cpp)

```cpp
class OneShotEvent {
 public:
  void Wait() {
    twist::ed::futex::Wait(fired_, /*old=*/0);
  }

  void Fire() {
    // _До_ записи в fired_
    auto wake_key = twist::ed::futex::PrepareWake(fired_);

    fired_.store(1);
    twist::ed::futex::WakeAll(wake_key);
  }

 private:
  // Wait работает только с atomic-ом для типа uint32_t
  twist::ed::std::atomic<uint32_t> fired_{0};
};
```

`Wait` перечитывает значение атомика после пробуждения, и если оно все еще равно `old`, то повторно паркует поток.

`Wake` – двухфазный: 
- сначала c помощью `PrepareWake` нужно получить ключ, адресующий связанную с атомиком очередь ожидания,
- затем разбудить потоки из очереди, передав в `Wake{One,All}` подготовленный ключ.

Вызывать `PrepareWake` следует **до записи** в атомик.

См. http://wg21.link/P2616.

#### Timed-версия

С помощью `futex::WaitTimed` можно встать в очередь ожидания с таймаутом:

```cpp
bool woken = futex::WaitTimed(state, kNotReady, 1s);
```

Если по истечению таймаута поток не был разбужен с помощью вызовов `WakeOne` / `WakeAll`, то он сам покинет очередь ожидания фьютекса.

Вызов возвращает
- `true`, если 1) поток был явно разбужен с помощью вызовов `WakeOne` / `WakeAll` **или** 2) произошел spurious wakeup
- `false`, если истек таймаут

Между таймаутом и явным пробуждением есть гонка, так что `false` следует интерпретировать так: таймаут точно истек.

### `thread_local`

#### Pointer

Заголовочный файл: `<twist/ed/static/thread_local/ptr.hpp>`

Вместо `static thread_local T* name` нужно использовать макрос `TWISTED_STATIC_THREAD_LOCAL_PTR(T, name)`.

В сборке с файберами за этим макросом будет находиться специальный контейнер, который,
- для каждого потока хранит собственный адрес
- повторяет поведение указателя
- инициализируется `nullptr`-ом

Пример: [examples/thread_local/main.cpp](/examples/thread_local/main.cpp)

#### Произвольное значение

Заголовочный файл: `<twist/ed/static/thread_local/var.hpp>`

Вместо `static thread_local T name` нужно использовать макрос `TWISTED_STATIC_THREAD_LOCAL_VAR(T, name)`:

```cpp
// ~ static thread_local Widget w;
TWISTED_STATIC_THREAD_LOCAL_VAR(Widget, w);

// Вызов метода Foo – через operator-> (как при работе с std::optional)
w->Foo();

// Получение ссылки на объект – через operator* (как при работе с std::optional)
(*w).Foo();

// Взятие адреса – operator&
Widget w_ptr = &w;
w_ptr->Foo();
```

### Отступления от `std` ⚠️

- `atomic` по умолчанию намеренно не поддерживает методы `wait` / `notify` в пользу более безопасных внешних функций ожидания / пробуждения
  - Включить поддержку `wait` можно с помощью флага CMake `TWIST_ATOMIC_WAIT=ON`
- default constructor для `atomic` помечен как `deprecated`: предпочитайте явную инициализацию

## Пример

Так будет выглядеть простейший Test-and-Set спинлок, написанный по правилам _Twist_:

```cpp
// Вместо #include <atomic>
#include <twist/ed/std/atomic.hpp>

#include <twist/ed/wait/spin.hpp>

class SpinLock {
 public:
  void Lock() {
    twist::ed::SpinWait spin_wait;
    while (locked_.exchange(true)) {
      spin_wait();
    }
  }
  
  void Unlock() {
    locked_.store(false);  // <-- Здесь работает fault injection:
                           // любая операция над примитивом синхронизации может привести к
                           // переключению исполнения на другой поток
  }
 private:
  // Вместо std::atomic<bool>
  // "Твистовый" atomic повторяет API std::atomic,
  // так что можно пользоваться стандартной документацией:
  // https://en.cppreference.com/w/cpp/atomic/atomic
  
  twist::ed::std::atomic<bool> locked_{false};
};
```

## Запуск кода

```cpp
#include <twist/run/cross.hpp>

#include <twist/ed/std/thread.hpp>

int main() {
  // Универсальный режим запуска: работает и с потоками, и с файберами
  twist::run::Cross([] {
    twist::ed::std::thread t([] {
      for (size_t i = 0; i < 7; ++i) {
        twist::ed::std::this_thread::yield();
      }
    });
  
    t.join();
  });
  
  return 0;
}
```
